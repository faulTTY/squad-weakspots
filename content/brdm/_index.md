---
title: "BRDM"
date: 2020-11-28T09:28:35+01:00
albumthumb: "brdm/brdm2_thumb.jpg"
resources:
- src: brdm/brdm_front.jpg
  alt: brdm front
  phototitle: BRDM-2 Front
  description: The front is well protected, but has a weak spot at the drivers seat.
- src: brdm/brdm_back.jpg
  alt: brdm back
  phototitle: BMP-1 Back
  description: The back is well protected, but has a weak spot on top.
- src: brdm/brdm_side.jpg
  alt: brdm side
  phototitle: BRDM-2 Side
  description: Side armor top has lowest armor.
- src: brdm/brdm_kill.jpg
  alt: brdm kill
  phototitle: BRDM-2 Kill Shot
  description: Shoot at the drivers seat for maximum effect.
---

