---
title: "BMP 2"
date: 2020-11-28T09:27:14+01:00
albumthumb: "bmp-2/Bmp2_thumb.jpg"
resources:
- src: bmp-2/bmp2_front.jpg
  alt: bmp1 front
  phototitle: BMP-2 Front
  description: Shoot the top of the front, but not the turret.
- src: bmp-2/bmp2_back.jpg
  alt: bmp1 back
  phototitle: BMP-2 Back
  description: The turret is well armored all around, but the back itself is weak.
- src: bmp-2/bmp2_side.jpg
  alt: bmp1 side
  phototitle: BMP-2 Side
  description: Side armor top has lowest armor.
---

