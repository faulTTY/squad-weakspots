---
title: "TAPV"
date: 2020-11-28T21:00:42+01:00
albumthumb: "tapv/tapv_thumb.jpg"
resources:
- src: tapv/tapv_front.jpg
  alt: tapv front
  phototitle: TAPV Front
  description: Front armor has a nice window, this is where you need to shoot.
- src: tapv/tapv_back.jpg
  alt: tapv back
  phototitle: TAPV Back
  description: Back armor has a door. Shoot here for maximum effect.
- src: tapv/tapv_side.jpg
  alt: tapv side
  phototitle: TAPV Side
  description: Side armors door can be shot through.
---

