---
title: "BTR80/82A"
date: 2020-11-28T09:31:14+01:00
albumthumb: "btr/btr_thumb.jpg"
resources:
- src: btr/btr_front.jpg
  alt: btr front
  phototitle: BTR 80/82A Front
  description: Lower front has the weakest armor.
- src: btr/btr_back.jpg
  alt: btr back
  phototitle: BTR 80/82A Back
  description: The back is easy to shoot through.
- src: btr/btr_side.jpg
  alt: btr side
  phototitle: BTR 80/82A Side
  description: Side armor bottom has lowest armor.
- src: btr/btr_kill.jpg
  alt: btr kill
  phototitle: BTR 80/82A Kill Shot
  description: Shoot the front just below the turret to get an easy kill.
---

