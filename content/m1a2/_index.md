---
title: "M1A2 Abrams"
date: 2020-11-28T09:34:51+01:00
albumthumb: "m1a2/m1a2_thumb.jpg"
resources:
- src: m1a2/m1a2_front.jpg
  alt: m1a2 front
  phototitle: M1A2 Abrams Front
  description: Like all other tanks, the area right around the tank cannon isn't as strong as the rest of the frontal armor.
- src: m1a2/m1a2_back.jpg
  alt: m1a2 back
  phototitle: M1A2 Abrams Back
  description: The back has the weakest armor, you can quickly shoot the engine and basically immobilize the tank.
- src: m1a2/m1a2_side.jpg
  alt: m1a2 side
  phototitle: M1A2 Abrams Side
  description: The disadvantage is the Abrams' turret, namely the poorly armored rear sides that are quite easy to hit if looking at it side-on. 
- src: m1a2/m1a2_kill.jpg
  alt: m1a2 kill
  phototitle: M1A2 Abrams Kill Shot
  description: It has a turret ring that can result in a one hit kill if hit by skilled (or lucky) gunners.
---

