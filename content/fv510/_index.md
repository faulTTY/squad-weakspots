---
title: "FV510 Warrior"
date: 2020-11-28T09:41:24+01:00
albumthumb: "fv510/fv510_thumb.jpg"
resources:
- src: fv510/fv510_front.jpg
  alt: fv510 front
  phototitle: FV510 Warrior Front
  description: Much like the Bradley, a frontal engagement with the Warrior in anything other than a main battle tank is not advised.
- src: fv510/fv510_back.jpg
  alt: fv510 back
  phototitle: FV510 Warrior Back
  description: No turret stabilization makes anything other than stationary shooting an unbelievable pain in the rear end for a vehicle's gunner. Try to get to it's back.
- src: fv510/fv510_side.jpg
  alt: fv510 side
  phototitle: FV510 Warrior Side
  description: The Warrior pictured here is the variant with extra side armor added. The added armor is Green strength while the Warrior hull beneath it is Blue strength.
- src: fv510/fv510_side2.jpg
  alt: fv510 side2
  phototitle: FV510 Warrior Side
  description: The Warrior pictured here is the variant with extra side armor added. The added armor is Green strength while the Warrior hull beneath it is Blue strength.
---

