---
title: "FV4034 Challenger 2"
date: 2020-11-28T09:39:36+01:00
albumthumb: "fv4034/fv4034_thumb.jpg"
resources:
- src: fv4034/fv4034_front.jpg
  alt: fv4034 front
  phototitle: FV4034 Challenger Front
  description: Try to hit the drivers window. If you can't aim well enough, shoot the lower front.
- src: fv4034/fv4034_back.jpg
  alt: fv4034 back
  phototitle: FV4034 Challenger Back
  description: The back has the weakest armor, you can quickly shoot the engine and basically immobilize the tank.
- src: fv4034/fv4034_side.jpg
  alt: fv4034 side
  phototitle: FV4034 Challenger Side
  description: The rear end of the turret is armored worst, but if you have the gunners attention, you'll never see it.
- src: fv4034/fv4034_kill.jpg
  alt: fv4034 kill
  phototitle: FV4034 Challenger Kill Shot
  description: The Challenger's biggest frontal weakness is its very large driver's window. An AP shot here has the potential to 1 hit the Challenger.
---

