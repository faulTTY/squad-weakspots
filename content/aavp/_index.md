---
title: "AAVP-7A1"
date: 2020-11-28T21:00:46+01:00
albumthumb: "aavp/aavp_thumb.jpg"
resources:
- src: aavp/aavp_front.jpg
  alt: aavp front
  phototitle: AAVP-7A1 Front
  description: 
- src: aavp/aavp_back.jpg
  alt: aavp back
  phototitle: AAVP-7A1 Back
  description: 
- src: aavp/aavp_side.jpg
  alt: aavp side
  phototitle: AAVP-7A1 Side
  description:
---

