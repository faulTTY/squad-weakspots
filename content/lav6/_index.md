---
title: "LAV 6.0"
date: 2020-11-28T21:00:46+01:00
albumthumb: "lav6/lav6_thumb.jpg"
resources:
- src: lav6/lav6_front.jpg
  alt: lav6 front
  phototitle: LAV 6.0 Front
  description: A frontal engagement in anything other than a MBT is ill adviced.
- src: lav6/lav6_back.jpg
  alt: lav6 back
  phototitle: LAV 6.0 Back
  description: Try to hit the door in the center of the back.
- src: lav6/lav6_side.jpg
  alt: lav6 side
  phototitle: LAV 6.0 Side
  description: The side has a large orange armored compartment on the backside of the vehicle. Hit there for maximum effect.
---

