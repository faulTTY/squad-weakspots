---
title: "T-72"
date: 2020-11-28T09:22:24+01:00
albumthumb: "t-72/t72_thumb.jpg"
resources:
- src: t-72/t72_front.jpg
  alt: t72 front
  phototitle: T-72 Front
  description: The T-72's most glaring weakness in a head-to-head fight is the square area around its turret. All other tanks have at least some armor in this area. The T-72 lacks even that - it has no armor around its main cannon. 
- src: t-72/t72_back.jpg
  alt: t72 back
  phototitle: T-72 Back
  description: The back has the weakest armor overall.
- src: t-72/t72_side.jpg
  alt: t72 side
  phototitle: T-72 Side
  description: The side is still Yellow rated, no dice here.
- src: t-72/t72_kill.jpg
  alt: t72 kill
  phototitle: T-72 Kill Shot
  description: Hitting here is the only reliable way to damage/kill a T-72 from the front. 
---

