---
title: "Leopard 2A6M"
date: 2020-11-28T21:00:38+01:00
albumthumb: "leo/leo_thumb.jpg"
resources:
- src: leo/leo_front.jpg
  alt: leo front
  phototitle: Leopard 2A6M Front
  description: A frontal engagement will only be successful if you hit the mounting point of the main armament.
- src: leo/leo_back.jpg
  alt: leo back
  phototitle: Leopard 2A6M Back
  description: The back can be shot even with medium weapons to take out the tanks engine quickly.
- src: leo/leo_side.jpg
  alt: leo side
  phototitle: Leopard 2A6M Side
  description: You can shoot the tracks to immobilize the tank in order to get to the rear end.
---

