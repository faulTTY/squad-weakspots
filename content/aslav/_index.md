---
title: "ASLAV 25"
date: 2020-11-28T09:33:08+01:00
albumthumb: "aslav/aslav_thumb.jpg"
resources:
- src: aslav/aslav_front.jpg
  alt: aslav front
  phototitle: ASLAV Front
  description: The ASLAV's lower frontal sloped armor is the strongest, so don't aim there. If looking at it head on, the engine is on the left. Hit there to blow the engine and smoke out the gunner's sight. 
- src: aslav/aslav_back.jpg
  alt: aslav back
  phototitle: ASLAV Back
  description: Back is red rated.
- src: aslav/aslav_side.jpg
  alt: aslav side
  phototitle: ASLAV Side
  description: Hits to the turret will also yield good results due to the ASLAV's poor armor there, especially the sides.
- src: aslav/aslav_kill.jpg
  alt: aslav kill
  phototitle: ASLAV Kill
  description: Aim for the turret ring of the ASLAV the same way you'd aim for the BTR-82's turret ring.
---

