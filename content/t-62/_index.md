---
title: "T-62"
date: 2020-11-28T09:19:33+01:00
albumthumb: "t-62/t62_thumb.jpg"
resources:
- src: t-62/t62_front.jpg
  alt: t62 front
  phototitle: T62 Front
  description: The T62, being a long outdated tank, is very weak to every other modern tank, including IFV's with 25mm or 30mm turrets. A shot to anywhere but the sides of the T-62's turret will yield high damage. 
- src: t-62/t62_back.jpg
  alt: t62 back
  phototitle: T62 Back
  description: The base of the T62's rear turret is surprisingly well armored, however the hull itself is not. Aim for the hull.
- src: t-62/t62_side.jpg
  alt: t62 side
  phototitle: T62 Side
  description: Aim the top half of the side armor for maximum effect.
- src: t-62/t62_kill.jpg
  alt: t62 kill
  phototitle: T62 Kill Shot
  description: An AP shot to the ammo compartment on the front right of the T-62 can ammo rack it, resulting in a 1 hit kill.
---

