---
title: "FV432 Bulldog"
date: 2020-11-28T09:45:53+01:00
albumthumb: "fv432/fv432_thumb.jpg"
resources:
- src: fv432/fv432_front.jpg
  alt: fv432 front
  phototitle: FV432 Bulldog Front
  description: The front has excellent and side armor.
- src: fv432/fv432_back.jpg
  alt: fv432 back
  phototitle: FV432 Bulldog Back
  description: Still has decent armor on the rear.
- src: fv432/fv432_side.jpg
  alt: fv432 side
  phototitle: FV432 Bulldog Side
  description: The front has excellent and side armor.
- src: fv432/fv432_kill.jpg
  alt: fv432 kill
  phototitle: FV432 Bulldog Kill Shot
  description: Rear armor hits with HMGs or explosives will also suffice, but the very large turret is the best target to go for.
- src: fv432/fv432_extra.jpg
  alt: fv432 extra
  phototitle: FV432 Bulldog Versions
  description: There are two types of Bulldog. The RWS version is utterly harmless to all but unarmored Technicals.
---

