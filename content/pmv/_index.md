---
title: "PMV"
date: 2020-11-28T09:33:08+01:00
albumthumb: "pmv/PMV_thumb.jpg"
resources:
- src: pmv/PMV_front.jpg
  alt: PMV front
  phototitle: PMV Front
  description: The PMV is orange rated all around with red rated windows you can penetrate with heavy machineguns and rockets.
- src: pmv/PMV_back.jpg
  alt: PMV back
  phototitle: PMV Back
  description: The PMV is orange rated all around with red rated windows you can penetrate with heavy machineguns and rockets.
- src: pmv/PMV_side.jpg
  alt: PMV side
  phototitle: PMV Side
  description: The PMV is orange rated all around with red rated windows you can penetrate with heavy machineguns and rockets.
- src: pmv/PMV_aim1.png
  alt: PMV aim1
  phototitle: PMV Aim Range
  description: The PMV's gunner sight is simple (and utterly confusing) at first sight. In fact it's so simple they forgot to add numbers to the aiming reticle.
- src: pmv/PMV_aim2.png
  alt: PMV aim2
  phototitle: PMV Aim Point of Impact
  description: Point of Impact
- src: pmv/PMV_aim3.png
  alt: PMV aim3
  phototitle: PMV Aim Example
  description: Example impact.
---

