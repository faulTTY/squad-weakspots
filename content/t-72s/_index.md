---
title: "T-72S"
date: 2020-11-28T09:46:11+01:00
albumthumb: "t-72s/t72s_thumb.jpg"
resources:
- src: t-72s/t72s_front.jpg
  alt: t72s front
  phototitle: T-72 S Front
  description: After doing some testing I came to the conclusion that the T-72 S is just as (nearly) impervious to a frontal assault as its base model.
- src: t-72s/t72s_back.jpg
  alt: t72s back
  phototitle: T-72 S Back
  description: The back has the weakest armor overall.
- src: t-72s/t72s_side.jpg
  alt: t72s side
  phototitle: T-72 S Side
  description: The MEA's T-72 has ERA (Explosive Reactive Armor) plates all over the front and side of it; they're the little square looking metal plates. 
- src: t-72s/t72s_kill.jpg
  alt: t72s kill
  phototitle: T-72 S Kill Shot
  description: Your best bet if you run into this tank head on is to aim at the turret mantle or knock its tracks off then flank around the side(s).
- src: t-72s/t72s_kill2.jpg
  alt: t72s kill
  phototitle: T-72 S Kill Shot From Top
  description: If you have elevation on the T-72 S and can see the top of its turret, make sure you aim carefully so your projectile doesn't hit the ERA plates. 
---

