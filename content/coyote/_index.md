---
title: "Coyote"
date: 2020-11-28T09:33:08+01:00
albumthumb: "coyote/coyote_thumb.jpg"
resources:
- src: coyote/aslav_front.jpg
  alt: aslav front
  phototitle: Coyote Front
  description: The Coyote's lower frontal sloped armor is the strongest, so don't aim there. If looking at it head on, the engine is on the left. Hit there to blow the engine and smoke out the gunner's sight. 
- src: coyote/aslav_back.jpg
  alt: aslav back
  phototitle: Coyote Back
  description: Back is red rated.
- src: coyote/aslav_side.jpg
  alt: aslav side
  phototitle: Coyote Side
  description: Hits to the turret will also yield good results due to the Coyote's poor armor there, especially the sides.
- src: coyote/aslav_kill.jpg
  alt: aslav kill
  phototitle: Coyote Kill
  description: Aim for the turret ring of the Coyote the same way you'd aim for the BTR-82's turret ring.
---

