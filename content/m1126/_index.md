---
title: "M1126 Stryker"
date: 2020-11-28T09:50:58+01:00
albumthumb: "m1126/m1126_thumb.jpg"
resources:
- src: m1126/m1126_front.jpg
  alt: m1126 front
  phototitle: M1126 Stryker Front
  description: The Stryker's biggest advantage as an APC is its Blue rated frontal armor. Unless you're in a tank or ATGM equipped IFV, you are very unlikely to win a fight against a Stryker head on.
- src: m1126/m1126_back.jpg
  alt: m1126 back
  phototitle: M1126 Stryker Back
  description: The back can be shot through relatively easily.
- src: m1126/m1126_side.jpg
  alt: m1126 side
  phototitle: M1126 Stryker Side
  description: The side's yellow armor is what you'll shoot the most. It still takes a long time to win the fight though.
- src: m1126/m1126_kill.jpg
  alt: m1126 kill
  phototitle: M1126 Stryker Kill Shot
  description:  If you have a shot on the Stryker's right side it's advisable to aim for the highlighted black grate, which contains the CROWS' ammo.
- src: m1126/m1126_kill2.jpg
  alt: m1126 kill2
  phototitle: M1126 Stryker Kill Shot
  description:  If you have a shot on the Stryker's right side it's advisable to aim for the highlighted black grate, which contains the CROWS' ammo.
---

