---
title: "M2A3 Bradley"
date: 2020-11-28T09:36:00+01:00
albumthumb: "m2a3/m2a3_thumb.jpg"
resources:
- src: m2a3/m2a3_front.jpg
  alt: m2a3 front
  phototitle: M2A3 Bradley Front
  description: In a frontal engagement the Bradley is what you might call "Unf*ckwithable." Unless you're in a T-72 you will not win a head-on fight with a Bradley -- and even then you might still lose if the Bradley has its 2 TOW missiles ready, armed, and waiting for you.
- src: m2a3/m2a3_back.jpg
  alt: m2a3 back
  phototitle: M2A3 Bradley Back
  description: The back can be shot through relatively easily, just don't go for the turret.
- src: m2a3/m2a3_side.jpg
  alt: m2a3 side
  phototitle: M2A3 Bradley Side
  description: The side is well armored unless you hit the weak spot where the backpacks are.
- src: m2a3/m2a3_kill.jpg
  alt: m2a3 kill
  phototitle: M2A3 Bradley Kill Shot
  description: There is one spot on its side that can yield great results if you've got an ATGM or skilled HAT trooper at your disposal. A penetrating hit there can ammo rack the Bradley and destroy it very quickly. 
---

