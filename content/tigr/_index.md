---
title: "TIGR"
date: 2020-11-28T09:33:08+01:00
albumthumb: "tigr/tigr_thumb.jpg"
resources:
- src: tigr/tigr_front.jpg
  alt: tigr front
  phototitle: TIGR Front
  description: If it's an open top Tigr just shoot the guy on the gun. If it's a remote operated Tigr and you only have small arms then you'll want to hide, or get your AT to hit it. If you're on an HMG or in a vehicle with an HMG on it, aim for the windows.
- src: tigr/tigr_back.jpg
  alt: tigr back
  phototitle: TIGR Back
  description: Orange rated armor all around.
- src: tigr/tigr_side.jpg
  alt: tigr side
  phototitle: TIGR Side
  description: Orange rated armor all around.
- src: tigr/tigr_side2.jpg
  alt: tigr side2
  phototitle: TIGR Side
  description: Orange rated armor all around.
- src: tigr/tigr_kill.jpg
  alt: tigr kill
  phototitle: TIGR Kill
  description: The gunner on the RWS variant is sitting in the back cab.
- src: tigr/tigr_kill2.jpg
  alt: tigr kill2
  phototitle: TIGR Kill2
  description: If looking at it from the front, he's behind the guy sitting in the front left seat.
---

