---
title: "M-ATV"
date: 2020-11-28T09:37:57+01:00
albumthumb: "matv/matv_thumb.jpg"
resources:
- src: matv/matv_kill.jpg
  alt: matv kill
  phototitle: M-ATV Kill Shot
  description: If it's an open top turret, aim for the turret and the guy manning it. If it's a CROWS then just shoot the main body until it explodes. 
- src: matv/matv_side.jpg
  alt: matv side
  phototitle: M-ATV Side
  description: It's Orange rated all around, except for the open top turret.
---

