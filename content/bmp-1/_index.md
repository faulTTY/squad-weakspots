---
title: "BMP 1"
date: 2020-11-28T09:27:10+01:00
albumthumb: "bmp-1/Bmp1_thumb.jpg"
resources:
- src: bmp-1/bmp1_front.jpg
  alt: bmp1 front
  phototitle: BMP-1 Front
  description: Shoot the top of the front, but not the turret, and definitely not the base of the main weapon.
- src: bmp-1/bmp1_back.jpg
  alt: bmp1 back
  phototitle: BMP-1 Back
  description: Back armor of the turret has the lowest armor.
- src: bmp-1/bmp1_side.jpg
  alt: bmp1 side
  phototitle: BMP-1 Side
  description: Side armor top has lowest armor.
---
