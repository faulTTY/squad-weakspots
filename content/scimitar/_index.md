---
title: "FV107 Scimitar"
date: 2020-11-28T09:33:08+01:00
albumthumb: "scimitar/scimitar_thumb.jpg"
resources:
- src: scimitar/scimitar_front.jpg
  alt: scimitar front
  phototitle: Scimitar Front
  description: Front is yellow rated
- src: scimitar/scimitar_back.jpg
  alt: scimitar back
  phototitle: Scimitar Back
  description: Back is red rated.
- src: scimitar/scimitar_side.jpg
  alt: scimitar side
  phototitle: Scimitar Side
  description: Side is yellow rated.
---

