---
title: "MT-LB Sh*tbox"
date: 2020-11-28T09:29:57+01:00
albumthumb: "mt-lb/mtlb_thumb.jpg"
resources:
- src: mt-lb/mt-lb_front.jpg
  alt: mt-lb front
  phototitle: MT-LB Sh*tbox Front
  description: When facing it from the front, the lower sloped armor is Red rated, so aiming there will especially hurt when hit.
- src: mt-lb/mt-lb_back.jpg
  alt: mt-lb back
  phototitle: MT-LB Sh*tbox Back
  description: The MTLB, surprisingly enough, has Blue rated armor on the back. This doesn't pose much of a problem for tanks with AP rounds or for ATGM missiles, but it may very well be a waste of a rocket for LAT and HAT troopers.
- src: mt-lb/mt-lb_side.jpg
  alt: mt-lb side
  phototitle: MT-LB Sh*tbox Side
  description: Affectionately known as the "Sh*tbox" it gets this nickname from its poor overall armor and underwhelming armaments against other armored vehicles.
- src: mt-lb/mt-lb_kill.jpg
  alt: mt-lb kill
  phototitle: MT-LB Sh*tbox Kill Shot
  description: Don't shoot the back, anything else will quickly dispose of this 'threat'.
---

